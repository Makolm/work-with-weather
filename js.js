const API_KEY // удалил свой ключ, для того чтобы его получить,
              // зарегестрируйтесь на сайте http://api.openweathermap.org

async function get_info_weather_with_API(city_name, where_need_find = null){

    let string_query = 'http://api.openweathermap.org/data/2.5/weather?q=' + city_name;

    if(where_need_find)
        string_query = string_query + ',' + where_need_find;
    
    string_query = string_query + '&appid=' + API_KEY;

    console.log(string_query)
    let response = await fetch(string_query);

    if(response.status !== 200){
        alert('error API');
        return null;
    }

    let object = await response.json();
    console.log(object)
    return object;

}

var info_about_city = get_info_weather_with_API('brest', 'fr'); // Город Лондон может быть не только в Англии
                                           // поэтому иногда можно использовать второй параметр
//давайте сравним два популярных города - Брест
/*
    get_info_weather_with_API('brest', 'by'); 
    или 
    get_info_weather_with_API('brest'); 
    по умолчанию город берется с Беларуси, либо мы можем прописать сами
    Беларусь(by), однако мы можем поставить fr, чтобы город Брест брался
    с Франции
*/